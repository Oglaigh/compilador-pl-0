package compilar;

import java.io.IOException;

public class AnalizadorSintactico {

    private final AnalizadorLexico aLex;
    private final AnalizadorSemantico aSem;
    private final GeneradorDeCodigo genCod;
    private final IndicadorDeErrores indicadorDeErrores;

    public AnalizadorSintactico(AnalizadorLexico aLex, AnalizadorSemantico aSem, GeneradorDeCodigo genCod, IndicadorDeErrores indicadorDeErrores) {
        this.aLex = aLex;
        this.aSem = aSem;
        this.genCod = genCod;
        this.indicadorDeErrores = indicadorDeErrores;
    }

    public void analizar() throws IOException {
        aLex.escanear();
        programa();
        genCod.volcar();
    }

    private void programa() throws IOException {
        do {
            aLex.escanear();
            System.out.println(aLex.getS() + ": " + aLex.getCad());
        } while (aLex.getS() != Terminal.EOF);

    }

}
